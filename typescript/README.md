# Setup
- This project uses Node 20.10.0.
  - You can use [nvm](https://github.com/nvm-sh/nvm) to easily manage different versions of node: `nvm use 20.10.0`
- `npm install`
- To run a script scripts, do: `npx tsm <path to your script here>`
  - Be sure to set any relevant environment variables (see config.ts for variable names) first