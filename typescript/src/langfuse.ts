import Langfuse from "langfuse-node";
import config from "./config";

export const getClient = () => {
    if (!client) {
        client = createClient()
    }
    return client
}

const createClient = (): Langfuse => {
    return new Langfuse({
        secretKey: config.langfuseSecretKey,
        publicKey: config.langfusePublicKey,
        baseUrl: config.langfuseBaseUrl,
    })
}

let client = createClient()

export enum LangfusePromptNameEnum {
    GET_SESSION_MESSAGE_MODERATION_DECISION = 'get-session-message-moderation-decision',
}

export enum DatasetItemStatusEnum {
    ARCHIVED = 'ARCHIVED',
    ACTIVE = 'ACTIVE',
}