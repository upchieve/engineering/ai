const config = {
    // langfuse
    langfuseSecretKey: process.env.SUBWAY_LANGFUSE_SECRET_KEY || 'bogus',
    langfusePublicKey: process.env.SUBWAY_LANGFUSE_PUBLIC_KEY || 'bogus',
    langfuseBaseUrl: process.env.SUBWAY_LANGFUSE_BASEURL || 'bogus',

    // OpenAI
    openAIApiKey: process.env.OPEN_AI_API_KEY || 'bogus',
}

export default config