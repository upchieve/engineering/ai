import { DatasetItemStatusEnum, getClient, LangfusePromptNameEnum } from "../../langfuse";
import { TextPromptClient } from 'langfuse-core'
import { LangfuseTraceClient } from "langfuse";
import Langfuse from "langfuse-node";
import  * as _ from 'lodash-es'
import {openai} from "../../openai";

interface ModerationDatum {
    id: string
    status: DatasetItemStatusEnum
    input: {
        message: string
        type: 'student' | 'volunteer'
    }
    expected: {
        reasons: { [key: string]: string[] }
        appropriate: boolean
    }
    link: (
        obj: LangfuseTraceClient,
        runName: string,
        runArgs?: {
            description?: string
            metadata?: any
        }
    ) => Promise<{
        id: string
    }>
}

type TestResult = ModerationDatum & {
    actual: {
        appropriate: boolean
        reasons: { [key: string]: string[] }
    }
}

/**
 * Pulls the moderation dataset from Langfuse and the latest production-tagged moderation prompt from Langfuse.
 *
 * Tests the prompt against all items in the dataset. Each AI generation is saved with a score determined by
 * {@link evaluate}, and the output is saved as an Experiment Run in Langfuse
 * (see https://langfuse.com/docs/datasets/python-cookbook#define-application-and-run-experiments).
 */
async function main(
    batchSize = 50,
    customRunName: string | undefined = undefined
) {
    const runName = customRunName ?? getRunName()
    const lfClient = getClient()
    const prompt = await lfClient.getPrompt(LangfusePromptNameEnum.GET_SESSION_MESSAGE_MODERATION_DECISION)
    console.log(`Evaluating the following prompt: ${prompt.prompt}`)

    const dataset = await getDataset(lfClient)
    const totalBatches = Math.ceil(dataset.length / batchSize)
    console.log(
        {
            datasetSize: dataset.length,
            runName,
            totalBatches,
        },
        'Beginning moderation testing'
    )

    const startTime = new Date()
    for (let i = 0; i < totalBatches; i++) {
        console.log(`Beginning moderation test batch ${i}/${totalBatches}`)
        await processBatch(
            dataset,
            prompt as TextPromptClient,
            i,
            batchSize,
            lfClient,
            runName
        )
    }
    const endTime = new Date()
    const elapsedTime = endTime.getTime() - startTime.getTime()
    console.log(`Completed moderation testing in ${elapsedTime} ms`)
    return runName
}

const processBatch = async (
    dataset: ModerationDatum[],
    prompt: TextPromptClient,
    batchNumber: number,
    batchSize: number,
    client: Langfuse,
    runName: string
): Promise<void> => {
    const startTime = new Date()
    const promises = []
    const start = batchNumber * batchSize
    const slice = dataset.slice(start, start + batchSize)
    for (const datum of slice) {
        promises.push(
            testAiDecision(datum, prompt, client, runName).catch(err => {
                console.warn(
                    err,
                    `Failed to run moderation test against item ${
                        (datum as ModerationDatum).id
                    }`
                )
            })
        )
    }
    const results = await Promise.all(promises)
    const endTime = new Date()
    const elapsedTime = endTime.getTime() - startTime.getTime()
    const failureCount = results.filter(r => r === undefined).length
    console.log(
        `Completed moderation test batch ${batchNumber} in ${elapsedTime} ms with ${failureCount} failures`
    )
}

/**
 * Return 1 if the moderation decision (true/false) of the AI-generated response matches the expected decision
 * from the labelled dataset, and 0 otherwise.
 */
const evaluate = (testResult: TestResult): number => {
    return testResult.actual.appropriate === testResult.expected.appropriate
        ? 1
        : 0
}

const getRunName = (): string => {
    return `moderation-evaluation-${new Date().toISOString()}`
}

/**
 * Call OpenAI to get the moderation decision for the given test case (`datum`).
 * Scores the result
 * Also creates a Trace and Generation in langfuse, scoring
 */
const testAiDecision = async (
    datum: ModerationDatum,
    prompt: TextPromptClient,
    client: Langfuse,
    runName: string
): Promise<TestResult> => {
    const scoreName = 'moderation-decision'
    const model = 'gpt-4o'
    const trace = client.trace({
        input: datum,
        name: 'moderation-evaluation',
    })
    const generation = trace.generation({
        prompt,
        model,
        input: datum.input,
    })

    const chatCompletion = await openai.chat.completions.create({
        model,
        messages: [
            {
                role: 'system',
                content: prompt.prompt,
            },
            {
                role: 'user',
                content: datum.input.message
            },
        ],
        response_format: { type: 'json_object' },
    })
    const decision = JSON.parse(chatCompletion.choices[0].message.content || '')
    const output = {
        reasons: decision.reasons,
        appropriate: decision.appropriate,
    }
    generation.end({
        output,
    })

    const testResult = {
        ...datum,
        actual: {
            appropriate: decision.appropriate,
            reasons: decision.reasons,
        },
    }
    // Link this generation to the run and score it
    const score = evaluate(testResult)
    datum.link(trace, runName)
    generation.score({
        name: scoreName,
        value: score,
    })
    trace.update({
        output,
        tags: score < 1 ? ['low-score'] : [],
    })
    return testResult
}

const getDataset = async (client: Langfuse): Promise<ModerationDatum[]> => {
    const datasetName = 'moderation'
    const res = await client.getDataset(datasetName)

    const items = res.items
        .filter(i => (i as any)['status'] === DatasetItemStatusEnum.ACTIVE)
        .map(
            i =>
                ({
                    id: i.id,
                    status: (i as any).status,
                    input: {
                        message: i.input.message,
                        type: i.input.type,
                    },
                    expected: {
                        appropriate: i.expectedOutput.appropriate,
                        reasons: i.expectedOutput.reasons,
                    },
                    link: i.link,
                } as ModerationDatum)
        )
    return _.uniqBy(items, i => `${i.input.message}-${i.input.type}`)
}

main()