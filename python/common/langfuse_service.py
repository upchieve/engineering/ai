from langfuse import Langfuse
import json
from common.config import config

langfuse = Langfuse(config['langfusePublicKey'], config['langfuseSecretKey'], config['langfuseHost'])

def fetch_dataset():
    response = langfuse.get_dataset('moderation')
    return response.items