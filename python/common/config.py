import os

config = {
    'langfusePublicKey': os.environ['SUBWAY_LANGFUSE_PUBLIC_KEY'],
    'langfuseSecretKey': os.environ['SUBWAY_LANGFUSE_SECRET_KEY'],
    'langfuseHost': os.environ['SUBWAY_LANGFUSE_BASEURL'],
    'openAiApiKey': os.environ['OPEN_AI_API_KEY']
}