"""
In this example, we demonstrate how you can use DSpy to improve an AI prompt that is used for moderating
chat messages.

Specifically, we:
- Use the Program from `moderation_program.py`
- Create an Optimizer object
- Create a metric function representing the metric we want to optimize the prompt for
- Attach examples to train the Optimizer
- Run the optimization and output the results to demonstrations.json

Relevant reading:
- Optimizers: https://dspy-docs.vercel.app/docs/building-blocks/optimizers
- Langfuse datasets: https://langfuse.com/docs/datasets/overview
"""
import dspy
from dspy.teleprompt import COPRO
import sys
import os
from moderation_program import PredictModeration
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from common.langfuse_service import fetch_dataset
from common.config import config
import time
import json

def optimize(metric_fn, program, training_set):
    """
    Run COPRO optimization on the program. Save results to demonstrations.json
    See COPRO docs: https://dspy-docs.vercel.app/docs/deep-dive/teleprompter/signature-optimizer#using-copro
    """
    optimizer = COPRO(metric=metric_fn)
    optimized_program = optimizer.compile(student=program, trainset=training_set, eval_kwargs=dict(num_threads=16, display_progress=True, display_table=0))
    optimized_program.save('demonstrations.json')

def validate_moderation_answer(example, prediction, trace=None):
    """
    The metric function to drive optimization
    """
    correct_answer = example['appropriate']
    try:
        actual = json.loads(prediction.answer)
        actual = actual['appropriate']
        print(f"metricFn: {correct_answer} =?= {actual} ? {correct_answer == actual}")
        result = correct_answer == actual
        if not result:
            print(f"Found failing test case: {prediction}")
        return result
    except:
        print(f"Metric function hit an exception on prediction {prediction}")
        return False

def build_training_set():
    """
    Fetch the dataset from Langfuse and form dspy Example objects
    """
    dataset = fetch_dataset()
    print("Fetched dataset from Langfuse.")

    def build_example(datum):
        return dspy.Example(message=datum.input['message'], user_type=datum.input['type'], appropriate=datum.expected_output['appropriate'], reasons=datum.expected_output['reasons']).with_inputs("message", "user_type")

    return [build_example(datum) for datum in dataset]

# Set your language model
lm = dspy.OpenAI(model='gpt-4o', api_key=config['openAiApiKey'])
# lm = dspy.OllamaLocal(model='phi3') # Alternatively, use a locally-hosted model with OllamaLocal
dspy.configure(lm=lm)

start = time.time()
training_set = build_training_set()
print(f'Created training set with {len(training_set)} items')
program = PredictModeration()
optimize(metric_fn=validate_moderation_answer, program=program, training_set=training_set)
end = time.time()
print("Finished optimization. Please examine demonstrations.json")
print(f"Time elapsed: {end - start} seconds")