"""
In this example, we create a DSpy Program for moderating chat messages.

Relevant reading:
- Signatures: https://dspy-docs.vercel.app/docs/deep-dive/signature/understanding-signatures
- Executing signatures: https://dspy-docs.vercel.app/docs/deep-dive/signature/executing-signatures
- Programs: https://dspy-docs.vercel.app/docs/cheatsheet#dspy-programs
"""

import dspy

class ModerateSignature(dspy.Signature): # Note: The docstring below serves as the actual core prompt content -- it's not just documentation!
    """
    You are moderating a tutoring chat room conversation between a student and an adult tutor. You are responsible for determining whether
    each message is appropriate.

    Inappropriate reasons include:
        - "profanity"
        - "hateful_language"
        - "email" - Use this reason when either participant is sharing an email address
        - "phone" - Use this reason when either participant is sharing a phone number
        - "other_contact_info" - Use this reason when the message includes other forms of contact information, such as social media handles, a participant's physical locations, etc
        - "safety" - Use this reason when the message is inappropriate because it indicates plans to communicate outside of the chat, or other potential for harm
        - "other" - Use this as a catch-all for anything else you thin is inappropriate for an adult to share with a minor or vice-a-versa

    Exceptions include:
        - Links to common text-based collaboration tools (like Google Docs) that are shared for the purpose of reviewing school assignments
        - Direct quotes from literature
        - Profanity that is likely just a typo

    Give your response as a JSON object containing the fields `message` (the original message), `appropriate` (boolean), and `reasons` (a mapping of the inappropriate reason to a list of relevant substrings from the message). Example:
    ```
    {
      "message": "call me at +18608889999",
      "appropriate": false,
      "reasons": {
        "phone": ["call me at +18608889999"]
      }
    }
    ```
    """
    message: str = dspy.InputField(desc="a single chat message from a chat log between a middle or high schooler tutee and an adult tutor")
    user_type: str = dspy.InputField(desc="Whether the user who sent the given message is a student or tutor")
    answer: dict = dspy.OutputField(desc="a parsable json string where `appropriate` is a boolean and `reasons` maps the 1-3 word inappropriate reason to relevant substrings from the message")

class PredictModeration(dspy.Module):
    def __init__(self):
        super().__init__()
        self.signature = ModerateSignature
        self.predictor = dspy.Predict(self.signature)

    def forward(self, message, user_type):
        result = self.predictor(message=message, user_type=user_type)
        answer = result.answer
        prefix = "```json"
        suffix = "```"
        start = answer.find(prefix)
        end = answer.find(suffix, start + len(prefix))
        answer = answer[start + len(prefix):end]
        return dspy.Prediction(answer=answer)