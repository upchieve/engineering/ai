# About this project
This directory contains Python scripts for working with **DSpy**. DSpy is a framework and Python library for programmatically optimizing
language model generations.

See:
- [DSpy repo](https://github.com/stanfordnlp/dspy?tab=readme-ov-file)
- [DSpy docs](https://dspy-docs.vercel.app/docs/intro)
- This short [paper](https://arxiv.org/abs/2406.11695)

# Setup
This project uses:
- Python 3
- [virtualenv](https://virtualenv.pypa.io/en/latest/index.html) for virtual environments
  - Note: _Do not_ commit your virtual environment, as it will not work on other contributors' machines
- pip and pipenv for package management
  - Note: _Do_ commit changes to Pipfile and Pipfile.lock

## One-time setup
1. Install dependencies on your machine:
   2. Python 3.12: download [here](https://www.python.org/downloads/) or do `brew install python@3.12`
   2. Pip: `python3 -m pip install --upgrade pip`
   2. virtualenv: follow instructions [here](https://virtualenv.pypa.io/en/latest/installation.html#installation)
2. From this directory, create a virtual environment: `virtualenv <virtualenv name here>`
2. Activate the virtual environment: From this directory, do `source <virtualenv name>/bin/activate`
3. Install pipenv in the virtualenv: `pip install pipenv`

## Recurring setup
Just remember to activate your virtual environment before you start running scripts! `source <virtualenv>/bin/activate`

## Installing packages
To install a package into your virtual environment:
1. Activate the virtual environment
2. Do `pipenv install` to install all packages from the Pipfile, and `pipenv install <package name>` to install a specific package
