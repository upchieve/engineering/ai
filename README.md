# About this project
This project contains tools and guidance for developing AI-powered features. This includes but is not limited to:
- Development workflow scripts and examples
  - Optimizing AI programs with DSpy
  - Evaluate AI generations against a Langfuse-hosted dataset
  - Etc

The project structure/organization is likely to evolve as we go. For now, it is broken out by language. We use Python for DSpy and typescript for everything else. See the READMEs in each directory for more information.